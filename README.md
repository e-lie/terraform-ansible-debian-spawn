# Terraform + ansible setup to spawn debian instances

- For adminsys teaching purpose
- Use scaleway and digitalocean cheap VPS
- Use the terraform-inventory tool get the ips dynamically for ansible
- Default to 10 scaleway START-XS + 5 digitalocean Basic (can be increased to 20 with a default account)

## Usage

- Install Terraform and Ansible
- Edit terraform.tfvars.dist to add your cloud provider credentials and sshkeys
- Rename it to terraform.tfvars (ignored by git for evident security purpose)

Then : 

1. `terraform init`
1. `terraform apply`
1. `ansible-playbook prepare.yml` ou `ansible-playbook yuno.yml`


