
variable "scaleway_api_token" {}
variable "scaleway_orga_id" {}
variable "do_api_token" {}
variable "do_sshkey_id" {}

provider "digitalocean" {
    token = "${var.do_api_token}"
}

provider "scaleway" {
  organization = "${var.scaleway_orga_id}"
  token        = "${var.scaleway_api_token}"
  region       = "par1"
}

resource "scaleway_ip" "mini_debs_ips" {
  count = 10
}

resource "scaleway_server" "mini_debs" {
  count = 10
  name  = "mini_deb_${count.index}"
  // Debian (mini) stretch (25GB) image id (got using curl on api GET /images)
  image = "b7382648-2a93-43bc-88e1-55a6f27ccd22"
  public_ip = "${element(scaleway_ip.mini_debs_ips.*.ip, count.index)}"
  type  = "START1-XS"
  # scaleway automatically add available ssh keys from the account to every server
}


resource "digitalocean_droplet" "do_debs" {
  count = 2
  name = "do-deb-${count.index}"
  image = "debian-9-x64"
  size = "1gb"
  region = "ams3"
  ssh_keys = ["${var.do_sshkey_id}"]
}

